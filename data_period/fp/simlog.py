from matplotlib import pyplot as plt
from func import read_file, derivative
import math
import numpy as np


# file = open('bt.fp.txt')

# file = open('lu.fp.txt')
file = open('bt_1000.0.fp.txt')
array_1 = read_file('bt_1.0.fp.txt')
array_10000 = read_file('bt_10000.0.fp.txt')
mr_array_1 = read_file('C://ibmpower8-sampling-data//data_period//mr//bt_1.0.mr.txt')
mr_array_10000 = read_file('C://ibmpower8-sampling-data//data_period//mr//bt_10000.0.mr.txt')


x = []
y = []
for line in file:
    temp = line.split()
    # print temp
    if float(temp[0]) > 0 and float(temp[1]):
        # if math.log(float(temp[0]), 2) < 15:
        #     x.append(math.log(float(temp[0]), 2))
        #     y.append(float(temp[1]))
# original data in x-axix
        if 0 < float(temp[0]) < 35000*128:
        	x.append(float(temp[0]))
        	y.append(float(temp[1]))

x = np.array(x)
y = np.array(y)
x_log = np.log(x)


z1 = np.polyfit(x, y, 5)
p1 = np.poly1d(z1)
der_p1 = p1.deriv()

y_log = 1066*np.power(x_log, 3) - 23970*np.power(x_log, 2) + 172100* x_log - 388700
print y_log


# log_x = np.log2(x)
# predict_base_x = range(0, 18)
# predict_x = np.power(2, predict_base_x)
# print predict_x


mr_x_axis = []
mr_x = []
# for i in range (0, 8194):
for i in range (0, 8194):
	temp = (p1 - 128 * i).roots
	for j in temp:
		if np.isreal(j) and j > 0:
			mr_x_axis.append(float(j)), mr_x.append(i * 128)
print "max , min = ", max(mr_x_axis), min(mr_x_axis)
# # mr_x_axis = np.array(mr_x_axis)
# print "der max min ", der_p1(187458.394545), der_p1(7.10031045876)
# mr_x_axis = np.array(mr_x_axis)
# for k in range (0, 8194):
#     mr_x.append(k * 128)
mr_x = np.array(mr_x)
print "len mr_x_axis", len(mr_x_axis)
print "len mr_x", len(mr_x)



plt.subplot(1,2,1)
plt.title("fp-bt", fontsize=30)
# plt.plot(np.log2(array_1[0] + 1), array_1[1])
# plt.plot(np.log2(array_10000[0] + 1), array_10000[1])
# plt.plot(np.log2(array_1[0] + 1), p1(array_1[0]))

plt.plot(np.log2(array_1[0] + 1), array_1[1])
plt.plot(np.log2(array_10000[0] + 1), array_10000[1])
plt.plot(np.log2(array_1[0] + 1), p1(array_1[0]))


plt.legend(['1/1', '1/10000', 'predict'], loc='upper left')
print 'p1 =\n', p1
print 'derive p1 =\n', der_p1
print array_1[0][-2], array_1[1][-2]
print array_10000[0][-2], array_10000[1][-2]
plt.xlim(0,35)
plt.ylim(0, 1400000)
plt.subplot(1,2,2)
plt.title("mr-bt", fontsize=30)
plt.plot(mr_array_1[0], mr_array_1[1])
plt.plot(mr_array_10000[0], mr_array_10000[1])
plt.plot(mr_x, der_p1(mr_x_axis))
plt.legend(['1/1', '1/10000', 'predict'], loc='upper left')
plt.xlabel('number of cacheline')
plt.ylabel('miss ratio')
# plt.savefig('predict_mr_is' + '.pdf')
# plt.plot(mr_array_1[0], mr_array_1[1])
# plt.plot(mr_array_10000[0], mr_array_10000[1])
# plt.xlim(0,128)
# plt.ylim(0, 30)

plt.show()

# print "x = ",mr_x_axis[1] ,mr_x_axis[-1]
# print der_p1(5282243), der_p1(5282243.394545)
# print p1(146), p1(187458.394545)

print (p1 - 128).roots
print (p1 - 256).roots
print (p1 - 12800).roots
print (p1 - 12928).roots