import numpy as np
import matplotlib.pyplot as plt
import math


def read_file(filename):

    with open(filename) as h:
        lines = h.readlines()

    x_axis = []
    y_axis = []

    for i in lines:
        line = i.split('\t')
        x_axis.append(float(line[0].rstrip(('\n'))))
        y_axis.append(float(line[1].rstrip(('\n'))))
    x_axis = np.array(x_axis)
    y_axis = np.array(y_axis)
    return [x_axis, y_axis]

    h.close()


# change the unit to Mb in x_axis while ploting footprint, multiply cache lenght then divide 2**20 convert to Mb from byte

def fp_trans(array):
    new_x_axis = [x / 16384 for x in array[0]]
    return [new_x_axis, array[1]]

# divide L1 cache miss ratio in y-axis
def fp_ft_trans(array, ratio):
    new_y_axis = [y / ratio for x in array[1]]
    # y_axis_ft.append(str(float(line[1].rstrip(('\n'))) / l1_mr))

# def plot_fp(fp_1, fp_10, fp_100, fp_1000, fp_10000, fp_ft)
# def plot_fp(fn_1, fn_10 = None, fn_100  = None, fn_1000 = None, fn_10000 = None, fn_ft = None, xlim = None, ylim = None):
#     array_fn_1 = read_file(fn_1)
#     array_fn_10 = read_file(fn_10)
#     array_fn_100 = read_file(fn_100)
#     array_fn_1000 = read_file(fn_1000)
#     array_fn_10000 = read_file(fn_10000)
#     array_fn_ft = read_file(fn_ft)

#     plt.plot(array_fn_1[0], array_fn_1[1], linewidth=2)
#     plt.plot(array_fn_10[0], array_fn_10[1], linewidth=2)
#     plt.plot(array_fn_100[0], array_fn_100[1], linewidth=2)
#     plt.plot(array_fn_1000[0], array_fn_1000[1], linewidth=2)
#     plt.plot(array_fn_10000[0], array_fn_10000[1], linewidth=2)
#     plt.plot(array_fn_ft[0], array_fn_ft[1], linewidth=2)
#     plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000', 'full trace'], loc='upper right', prop={'size':6})
#     plt.xlabel('window size')
#     plt.ylabel('miss ratio')
#     plt.xlim(0, xlim)
#     plt.show()
#     print fn_1

#     # plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000', 'full trace'], loc='upper right', prop={'size':10})
#     # # plt.title('mr-' + fn_10[0]+fn_10[1], fontsize=30)
#     # plt.xlabel('window size')
#     # plt.ylabel('miss ratio')
#     # xticks(x_axis_1)
#     # plt.ylim((0,1))
#     # plt.savefig('mr_ft_' + fn_10[0]+fn_10[1]+ '.pdf')
#     # plt.show()
# plot_fp("C://ibmpower8-sampling-data - Copy//data_period//fp//bt_1.0.fp.txt",
#   "C://ibmpower8-sampling-data - Copy//data_period//fp//bt_10.0.fp.txt",
#   "C://ibmpower8-sampling-data - Copy//data_period//fp//bt_100.0.fp.txt", 
#   "C://ibmpower8-sampling-data - Copy//data_period//fp//bt_1000.0.fp.txt",
#   "C://ibmpower8-sampling-data - Copy//data_period//fp//bt_10000.0.fp.txt",
#   "C://ibmpower8-sampling-data - Copy//data_period//fp//bt.fp.txt"
#   )