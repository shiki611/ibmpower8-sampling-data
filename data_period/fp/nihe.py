from func import read_file, fp_trans
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np

array_10000 = read_file('bt_10000.0.fp.txt')
array_1 = read_file('bt_1.0.fp.txt')
array_full = read_file('bt.fp.txt')

x = []
y = []
pre_x = np.array(array_10000[0])

# set a range of validate data
# variable here to change the learning lenght of lower sampling rate
for i in range(0, 1176):
	if array_1[0][i] > 0 :
		x.append(array_10000[0][i])
		y.append(array_10000[1][i])

x = np.array(x)
y = np.array(y)

log_x = np.log(x)
# change the order here
z_log = np.polyfit(log_x, y , 6)
p_log = np.poly1d(z_log)



print z_log, "plog----\n", p_log
# print z_log
# print len(pre_x), len(x)


plt.plot(array_1[0], array_1[1], 'b')
plt.plot(array_10000[0], array_10000[1], 'r')
plt.plot(array_full[0], array_full[1], 'y')
plt.plot(pre_x, np.polyval(p_log, np.log(pre_x)),'g')
plt.legend(['1/1', '1/10000', 'full', 'predict'], loc='upper left')

plt.xlim(0,100000000)
plt.ylim(0,2000000)
plt.show()