import matplotlib.pyplot as plt
import math

def plot(fn_1, fn_10, fn_100, fn_1000, fn_10000, fn_ft):
    with open(fn_1) as h:
        lines = h.readlines()

    x_axis_1 = []
    y_axis_1 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1.append(str(math.log(float(line[0])+1, 2)))
        y_axis_1.append((float(line[1].rstrip(('\n')))))
    h.close()

    with open(fn_10) as f:
        lines = f.readlines()

    x_axis_10 = []
    y_axis_10 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10.append(str(math.log(float(line[0])+1, 2)))
        y_axis_10.append((float(line[1].rstrip(('\n')))))
    f.close()

    with open(fn_100) as g:
        lines = g.readlines()

    x_axis_100 = []
    y_axis_100 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100.append(str(math.log(float(line[0])+1, 2)))
        y_axis_100.append((float(line[1].rstrip(('\n')))))
    g.close()

    with open(fn_1000) as h:
        lines = h.readlines()
    x_axis_1000 = []
    y_axis_1000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0])+1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    h.close()

    with open(fn_10000) as j:
        lines = j.readlines()
    x_axis_10000 = []
    y_axis_10000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10000.append(str(math.log(float(line[0])+1, 2)))
        y_axis_10000.append((float(line[1].rstrip(('\n')))))
    j.close()

    with open(fn_ft) as k:
        lines = k.readlines()
    x_axis_ft = []
    y_axis_ft = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_ft.append(str(math.log(float(line[0])+1, 2)))
        y_axis_ft.append((float(line[1].rstrip(('\n')))))
    k.close()

    plt.plot(x_axis_1, y_axis_1, linewidth=2)
    plt.plot(x_axis_10, y_axis_10, linewidth=2)
    plt.plot(x_axis_100, y_axis_100, linewidth=2)
    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10000, y_axis_10000, linewidth=2)
    plt.plot(x_axis_ft, y_axis_ft, linewidth=2)

    plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000', 'full trace'], loc='upper left', prop={'size':10})
    plt.title("fp-" + fn_10[0] + fn_10[1], fontsize=30)
    # xticks(x_axis_1)
    plt.xlabel('window size (logw)')
    plt.ylabel('access')
    # plt.savefig('fp_ft_' + fn_10[0] + fn_10[1] + '.pdf')
    plt.show()



# plot('bt_1.0.fp.txt', 'bt_10.0.fp.txt', 'bt_100.0.fp.txt', 'bt_1000.0.fp.txt', 'bt_10000.0.fp.txt', 'bt.fp.txt')
# plot('cg_1.0.fp.txt', 'cg_10.0.fp.txt', 'cg_100.0.fp.txt', 'cg_1000.0.fp.txt', 'cg_10000.0.fp.txt', 'cg.fp.txt')
# plot('dc_1.0.fp.txt', 'dc_10.0.fp.txt', 'dc_100.0.fp.txt', 'dc_1000.0.fp.txt', 'dc_10000.0.fp.txt', 'dc.fp.txt')
# # plot('ep_1.0.fp.txt', 'ep_10.0.fp.txt', 'ep_100.0.fp.txt', 'ep_1000.0.fp.txt', 'ep_10000.0.fp.txt', 'ep.fp.txt')
# plot('ft_1.0.fp.txt', 'ft_10.0.fp.txt', 'ft_100.0.fp.txt', 'ft_1000.0.fp.txt', 'ft_10000.0.fp.txt', 'ft.fp.txt')
plot('is_1.0.fp.txt', 'is_10.0.fp.txt', 'is_100.0.fp.txt', 'is_1000.0.fp.txt', 'is_10000.0.fp.txt', 'is.fp.txt')
# plot('lu_1.0.fp.txt', 'lu_10.0.fp.txt', 'lu_100.0.fp.txt', 'lu_1000.0.fp.txt', 'lu_10000.0.fp.txt', 'lu.fp.txt')
# plot('mg_1.0.fp.txt', 'mg_10.0.fp.txt', 'mg_100.0.fp.txt', 'mg_1000.0.fp.txt', 'mg_10000.0.fp.txt', 'mg.fp.txt')
# plot('sp_1.0.fp.txt', 'sp_10.0.fp.txt', 'sp_100.0.fp.txt', 'sp_1000.0.fp.txt', 'sp_10000.0.fp.txt', 'sp.fp.txt')
# plot('ua_1.0.fp.txt', 'ua_10.0.fp.txt', 'ua_100.0.fp.txt', 'ua_1000.0.fp.txt', 'ua_10000.0.fp.txt', 'ua.fp.txt')
