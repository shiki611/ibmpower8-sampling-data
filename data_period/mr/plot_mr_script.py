import matplotlib.pyplot as plt

def plot(fn_1, fn_10, fn_100, fn_1000, fn_10000):
    
    with open(fn_1) as h:
        lines = h.readlines()

    x_axis_1 = []
    y_axis_1 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1.append(str(float(line[0].rstrip(('\n'))) / 1))
        y_axis_1.append(line[1].rstrip(('\n')))
    print "1 mr = " + y_axis_1[-1]
    h.close()
    
    with open(fn_10) as f:
        lines = f.readlines()

    x_axis_10 = []
    y_axis_10 = []

    for i in lines:
            # print i
        line = i.split('\t')
        x_axis_10.append(str(float(line[0].rstrip(('\n'))) / 1))
        y_axis_10.append(line[1].rstrip(('\n')))
    print "10 mr = " + y_axis_10[-1]
    f.close()

    with open(fn_100) as g:
        lines = g.readlines()

    x_axis_100 = []
    y_axis_100 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100.append(str(float(line[0].rstrip(('\n'))) / 1))
        y_axis_100.append(line[1].rstrip(('\n')))
    print "100 mr = " + y_axis_100[-1]
    g.close()

    with open(fn_1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(float(line[0].rstrip(('\n'))) / 1))
        y_axis_1000.append(line[1].rstrip(('\n')))
    print "1000 mr = " + y_axis_1000[-1]
    h.close()

    with open(fn_10000) as j:
        lines = j.readlines()

    x_axis_10000 = []
    y_axis_10000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        # x_axis_10000.append(str(float(line[0].rstrip(('\n'))) / 1))
        x_axis_10000.append(str(float(line[0].rstrip(('\n'))) ))
        y_axis_10000.append(line[1].rstrip(('\n')))
    print "10000 mr = " + y_axis_10000[-1]
    j.close()

    plt.plot(x_axis_1, y_axis_1, linewidth=2)
    plt.plot(x_axis_10, y_axis_10, linewidth=2)
    plt.plot(x_axis_100, y_axis_100, linewidth=2)
    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10000, y_axis_10000, linewidth=2)

    plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000'], loc='upper right', prop={'size':10})
    plt.title('mr-' + fn_10[0]+fn_10[1], fontsize=30)
    # plt.xlabel('cache size (Mb)')
    plt.xlabel('number of cache line')
    plt.ylabel('miss ratio')
    # xticks(x_axis_1)
    plt.savefig('mr_' + fn_10[0]+fn_10[1]+ '.pdf')
    plt.show()



plot('bt_1.0.mr.txt', 'bt_10.0.mr.txt', 'bt_100.0.mr.txt', 'bt_1000.0.mr.txt', 'bt_10000.0.mr.txt')
plot('cg_1.0.mr.txt', 'cg_10.0.mr.txt', 'cg_100.0.mr.txt', 'cg_1000.0.mr.txt', 'cg_10000.0.mr.txt')
plot('dc_1.0.mr.txt', 'dc_10.0.mr.txt', 'dc_100.0.mr.txt', 'dc_1000.0.mr.txt', 'dc_10000.0.mr.txt')
plot('ep_1.0.mr.txt', 'ep_10.0.mr.txt', 'ep_100.0.mr.txt', 'ep_1000.0.mr.txt', 'ep_10000.0.mr.txt')
plot('ft_1.0.mr.txt', 'ft_10.0.mr.txt', 'ft_100.0.mr.txt', 'ft_1000.0.mr.txt', 'ft_10000.0.mr.txt')
plot('is_1.0.mr.txt', 'is_10.0.mr.txt', 'is_100.0.mr.txt', 'is_1000.0.mr.txt', 'is_10000.0.mr.txt')
plot('lu_1.0.mr.txt', 'lu_10.0.mr.txt', 'lu_100.0.mr.txt', 'lu_1000.0.mr.txt', 'lu_10000.0.mr.txt')
plot('mg_1.0.mr.txt', 'mg_10.0.mr.txt', 'mg_100.0.mr.txt', 'mg_1000.0.mr.txt', 'mg_10000.0.mr.txt')
plot('sp_1.0.mr.txt', 'sp_10.0.mr.txt', 'sp_100.0.mr.txt', 'sp_1000.0.mr.txt', 'sp_10000.0.mr.txt')
plot('ua_1.0.mr.txt', 'ua_10.0.mr.txt', 'ua_100.0.mr.txt', 'ua_1000.0.mr.txt', 'ua_10000.0.mr.txt')
