import matplotlib.pyplot as plt
import math

def plot(fn_1, fn_10, fn_100, fn_1000, fn_10000, ratio1, ratio2, ratio3, ratio4):
    with open(fn_1) as h:
        lines = h.readlines()

    x_axis_1 = []
    y_axis_1 = []
    nor_y_axis_1 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1.append(str(math.log(float(line[0]) * ratio1 +1, 2)))
        y_axis_1.append((float(line[1].rstrip(('\n')))))
    sum_1 = sum(y_axis_1)
    # nor_y_axis_1 = y_axis_1/sum_1
    for j in y_axis_1:
        nor_y_axis_1.append(j / sum_1)
    print 'sum_1 = ', sum_1
    # print y_axis_1
    print "-----------"
    print sum(nor_y_axis_1)

    h.close()

    with open(fn_10) as f:
        lines = f.readlines()

    x_axis_10 = []
    y_axis_10 = []
    nor_y_axis_10 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10.append(str(math.log(float(line[0]) * ratio2 +1, 2)))
        y_axis_10.append((float(line[1].rstrip(('\n')))))
    sum_10 = sum(y_axis_10)
    print 'sum_10 = ', sum_10
    for j in y_axis_10:
        # j = j / sum_10
        nor_y_axis_10.append(j / sum_10)
    # nor_y_axis_10 = y_axis_10/sum_10
    print sum(nor_y_axis_10)
    f.close()

    with open(fn_100) as g:
        lines = g.readlines()

    x_axis_100 = []
    y_axis_100 = []
    nor_y_axis_100 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100.append(str(math.log(float(line[0]) * ratio3 +1, 2)))
        y_axis_100.append((float(line[1].rstrip(('\n')))))
    sum_100 = sum(y_axis_100)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_100 = ', sum_100
    for j in y_axis_100:
        nor_y_axis_100.append(j / sum_100)
    print "----------------------"
    print sum(nor_y_axis_100)
    g.close()

    with open(fn_1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []
    nor_y_axis_1000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0]) * ratio4 +1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    sum_1000 = sum(y_axis_1000)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_1000 =', sum_1000
    for j in y_axis_1000:
        nor_y_axis_1000.append(j / sum_1000)
    print "----------------------"
    print sum(nor_y_axis_1000)
    h.close()

    with open(fn_10000) as l:
        lines = l.readlines()

    x_axis_10000 = []
    y_axis_10000 = []
    nor_y_axis_10000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10000.append(str(math.log(float(line[0]) +1 , 2)))
        y_axis_10000.append((float(line[1].rstrip(('\n')))))
    sum_10000 = sum(y_axis_10000)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_10000 =', sum_10000
    for j in y_axis_10000:
        nor_y_axis_10000.append(j / sum_10000)
    print "----------------------"
    print sum(nor_y_axis_10000)
    l.close()



    plt.plot(x_axis_1, nor_y_axis_1, linewidth=2)
    plt.plot(x_axis_10, nor_y_axis_10, linewidth=2)
    plt.plot(x_axis_100, nor_y_axis_100, linewidth=2)
    plt.plot(x_axis_1000, nor_y_axis_1000, linewidth=2)
    plt.plot(x_axis_10000, nor_y_axis_10000, linewidth=2)

    plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000'], loc='upper right', prop={'size':10})
    plt.title("RT-ns-" + fn_10[0] + fn_10[1], fontsize=30)
    # xticks(x_axis_1)
    plt.xlabel('window size (logw)')
    plt.ylabel('ratio(access/total access)')
    # plt.savefig('rt_ns_' + fn_10[0] + fn_10[1] + '.pdf')
    plt.show()



plot('bt_1.0.rt.txt', 'bt_10.0.rt.txt', 'bt_100.0.rt.txt', 'bt_1000.0.rt.txt', 'bt_10000.0.rt.txt', 9.955424379, 2.048706549, 1.212900796, 0.999699182)
plot('cg_1.0.rt.txt', 'cg_10.0.rt.txt', 'cg_100.0.rt.txt', 'cg_1000.0.rt.txt', 'cg_10000.0.rt.txt', 12.83168294, 2.364528295, 2.127354878, 1.069569033)
plot('dc_1.0.rt.txt', 'dc_10.0.rt.txt', 'dc_100.0.rt.txt', 'dc_1000.0.rt.txt', 'dc_10000.0.rt.txt', 146.0691034, 14.87517509, 2.182196107, 1.903615407)
plot('ep_1.0.rt.txt', 'ep_10.0.rt.txt', 'ep_100.0.rt.txt', 'ep_1000.0.rt.txt', 'ep_10000.0.rt.txt', 166.2396072, 16.59406062, 1.765193403, 1.468193717)
plot('ft_1.0.rt.txt', 'ft_10.0.rt.txt', 'ft_100.0.rt.txt', 'ft_1000.0.rt.txt', 'ft_10000.0.rt.txt', 3.101887689, 1.797911548, 1.195343058, 0.956064108)
plot('is_1.0.rt.txt', 'is_10.0.rt.txt', 'is_100.0.rt.txt', 'is_1000.0.rt.txt', 'is_10000.0.rt.txt', 36.07601899, 10.21287221, 3.436378972, 1.135123743)
plot('lu_1.0.rt.txt', 'lu_10.0.rt.txt', 'lu_100.0.rt.txt', 'lu_1000.0.rt.txt', 'lu_10000.0.rt.txt', 7.941013104, 2.029783386, 1.335526559, 1.024807175)
plot('mg_1.0.rt.txt', 'mg_10.0.rt.txt', 'mg_100.0.rt.txt', 'mg_1000.0.rt.txt', 'mg_10000.0.rt.txt', 28.44679939, 3.454807377, 2.022308554, 0.970826489)
plot('sp_1.0.rt.txt', 'sp_10.0.rt.txt', 'sp_100.0.rt.txt', 'sp_1000.0.rt.txt', 'sp_10000.0.rt.txt', 11.96449815, 2.700665821, 1.521910374, 1.010942565)
plot('ua_1.0.rt.txt', 'ua_10.0.rt.txt', 'ua_100.0.rt.txt', 'ua_1000.0.rt.txt', 'ua_10000.0.rt.txt', 35.92516436, 5.536897555, 2.774854736, 1.214933926)

# append(stry_axis_1(math.log(float(line[0])+1)))
