import matplotlib.pyplot as plt
import math

def plot(fn_1, fn_10, fn_100, fn_1000, fn_10000, fn_ft, acc_1, acc_10, acc_100, acc_1000, acc_10000, acc_ft):
    with open(fn_1) as h:
        lines = h.readlines()

    x_axis_1 = []
    y_axis_1 = []
    nor_y_axis_1 = []
    acc_nor_y_axis_1 = []
    acc_sum_1 = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1.append(str(math.log(float(line[0]) * acc_ft / acc_1 +1, 2)))
        # x_axis_1.append(str(math.log(float(line[0]) * acc_1 / acc_1 +1, 2)))
        y_axis_1.append((float(line[1].rstrip(('\n')))))
    sum_1 = sum(y_axis_1)
    # nor_y_axis_1 = y_axis_1/sum_1
    for j in y_axis_1:
        nor_y_axis_1.append(j / sum_1)
    print 'sum_1 = ', sum_1
    # print y_axis_1
    print "-----------"
    print sum(nor_y_axis_1)
    for k in nor_y_axis_1:
        acc_nor_y_axis_1.append(acc_sum_1 - k)
        acc_sum_1 = acc_sum_1 - k

    h.close()

    with open(fn_10) as f:
        lines = f.readlines()

    x_axis_10 = []
    y_axis_10 = []
    nor_y_axis_10 = []
    acc_nor_y_axis_10 = []
    acc_sum_10 = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10.append(str(math.log(float(line[0]) * acc_ft / acc_10 +1, 2)))
        # x_axis_10.append(str(math.log(float(line[0]) * acc_1 / acc_10 +1, 2)))
        y_axis_10.append((float(line[1].rstrip(('\n')))))
    sum_10 = sum(y_axis_10)
    print 'sum_10 = ', sum_10
    for j in y_axis_10:
        # j = j / sum_10
        nor_y_axis_10.append(j / sum_10)
    # nor_y_axis_10 = y_axis_10/sum_10
    print sum(nor_y_axis_10)
    for k in nor_y_axis_10:
        acc_nor_y_axis_10.append(acc_sum_10 - k)
        acc_sum_10 = acc_sum_10 - k
    f.close()

    with open(fn_100) as g:
        lines = g.readlines()

    x_axis_100 = []
    y_axis_100 = []
    nor_y_axis_100 = []
    acc_nor_y_axis_100 = []
    acc_sum_100 = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100.append(str(math.log(float(line[0]) * acc_ft / acc_100 +1, 2)))
        # x_axis_100.append(str(math.log(float(line[0]) * acc_1 / acc_100 +1, 2)))
        y_axis_100.append((float(line[1].rstrip(('\n')))))
    sum_100 = sum(y_axis_100)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_100 = ', sum_100
    for j in y_axis_100:
        nor_y_axis_100.append(j / sum_100)
    print "----------------------"
    print sum(nor_y_axis_100)
    for k in nor_y_axis_100:
        acc_nor_y_axis_100.append(acc_sum_100 - k)
        acc_sum_100 = acc_sum_100 - k
    g.close()

    with open(fn_1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []
    nor_y_axis_1000 = []
    acc_nor_y_axis_1000 = []
    acc_sum_1000 = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0]) * acc_ft / acc_1000 +1, 2)))
        # x_axis_1000.append(str(math.log(float(line[0]) * acc_1 / acc_1000 +1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    sum_1000 = sum(y_axis_1000)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_1000 =', sum_1000
    for j in y_axis_1000:
        nor_y_axis_1000.append(j / sum_1000)
    print "----------------------"
    print sum(nor_y_axis_1000)
    for k in nor_y_axis_1000:
        acc_nor_y_axis_1000.append(acc_sum_1000 - k)
        acc_sum_1000 = acc_sum_1000 - k
    h.close()

    with open(fn_10000) as l:
        lines = l.readlines()

    x_axis_10000 = []
    y_axis_10000 = []
    nor_y_axis_10000 = []
    acc_nor_y_axis_10000 = []
    acc_sum_10000 = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10000.append(str(math.log(float(line[0]) * acc_ft / acc_10000 +1, 2)))
        # x_axis_10000.append(str(math.log(float(line[0]) * acc_1 / acc_10000 +1, 2)))
        y_axis_10000.append((float(line[1].rstrip(('\n')))))
    sum_10000 = sum(y_axis_10000)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_10000 =', sum_10000
    for j in y_axis_10000:
        nor_y_axis_10000.append(j / sum_10000)
    print "----------------------"
    print sum(nor_y_axis_10000)
    for k in nor_y_axis_10000:
        acc_nor_y_axis_10000.append(acc_sum_10000 - k)
        acc_sum_10000 = acc_sum_10000 - k
    l.close()

    with open(fn_ft) as m:
        lines = m.readlines()

    x_axis_ft = []
    y_axis_ft = []
    nor_y_axis_ft = []
    acc_nor_y_axis_ft = []
    acc_sum_ft = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_ft.append(str(math.log(float(line[0]) +1 , 2)))
        y_axis_ft.append((float(line[1].rstrip(('\n')))))
    sum_ft = sum(y_axis_ft)
    # nor_y_axis_100 = y_axis_100/sum_10
    print 'sum_ft =', sum_ft
    for j in y_axis_ft:
        nor_y_axis_ft.append(j / sum_ft)
    print "----------------------"
    print sum(nor_y_axis_ft)
    for k in nor_y_axis_ft:
        acc_nor_y_axis_ft.append(acc_sum_ft - k)
        acc_sum_ft = acc_sum_ft - k
    m.close()

    # plt.plot(x_axis_1, nor_y_axis_1, linewidth=2)
    # plt.plot(x_axis_10, nor_y_axis_10, linewidth=2)
    # plt.plot(x_axis_100, nor_y_axis_100, linewidth=2)
    # plt.plot(x_axis_1000, nor_y_axis_1000, linewidth=2)
    # plt.plot(x_axis_10000, nor_y_axis_10000, linewidth=2)
    # plt.plot(x_axis_ft, nor_y_axis_ft, linewidth=2)

    plt.plot(x_axis_1, acc_nor_y_axis_1, linewidth=2)
    plt.plot(x_axis_10, acc_nor_y_axis_10, linewidth=2)
    plt.plot(x_axis_100, acc_nor_y_axis_100, linewidth=2)
    plt.plot(x_axis_1000, acc_nor_y_axis_1000, linewidth=2)
    plt.plot(x_axis_10000, acc_nor_y_axis_10000, linewidth=2)
    plt.plot(x_axis_ft, acc_nor_y_axis_ft, linewidth=2)

    plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000', 'full trace'], loc='upper right', prop={'size':10})
    plt.title("RT-acc-ns-" + fn_10[0] + fn_10[1], fontsize=30)
    # xticks(x_axis_1)
    plt.xlabel('window size (logw)')
    plt.ylabel('ratio(access/total access)')
    # plt.savefig('rt_acc_ft_ns_' + fn_10[0] + fn_10[1] + '.pdf')
    plt.show()



plot('bt_1.0.rt.txt', 'bt_10.0.rt.txt', 'bt_100.0.rt.txt', 'bt_1000.0.rt.txt', 'bt_10000.0.rt.txt', 'bt.rt.txt', 73420986, 73443079, 60533381, 35837727, 7374973, 920531000877 )
# plot('cg_1.0.rt.txt', 'cg_10.0.rt.txt', 'cg_100.0.rt.txt', 'cg_1000.0.rt.txt', 'cg_10000.0.rt.txt', 'cg.rt.txt', 33832567, 31631962, 15903584, 14308379, 2636643, 88293856201 )
# plot('dc_1.0.rt.txt', 'dc_10.0.rt.txt', 'dc_100.0.rt.txt', 'dc_1000.0.rt.txt', 'dc_10000.0.rt.txt', 'dc.rt.txt', 301735385, 158506484, 138271434, 20284493, 2065703, 332289621654 )
# # plot('ep_1.0.rt.txt', 'ep_10.0.rt.txt', 'ep_100.0.rt.txt', 'ep_1000.0.rt.txt', 'ep_10000.0.rt.txt', 'ep.rt.txt', 50374092, 34310249, 28537435, 3035670, 303021, 303021 )
# plot('ft_1.0.rt.txt', 'ft_10.0.rt.txt', 'ft_100.0.rt.txt', 'ft_1000.0.rt.txt', 'ft_10000.0.rt.txt', 'ft.rt.txt', 13467990, 14086911, 11267050, 7490908, 4341869, 79262791475 )
# plot('is_1.0.rt.txt', 'is_10.0.rt.txt', 'is_100.0.rt.txt', 'is_1000.0.rt.txt', 'is_10000.0.rt.txt', 'is.rt.txt', 1580779, 1392605, 460013, 154783, 43818, 5389788804 )
# plot('lu_1.0.rt.txt', 'lu_10.0.rt.txt', 'lu_100.0.rt.txt', 'lu_1000.0.rt.txt', 'lu_10000.0.rt.txt', 'lu.rt.txt', 46263850, 45143956, 34640906, 22792506, 5825938, 351185143333 )
# plot('mg_1.0.rt.txt', 'mg_10.0.rt.txt', 'mg_100.0.rt.txt', 'mg_1000.0.rt.txt', 'mg_10000.0.rt.txt', 'mg.rt.txt', 2654826, 2734604, 1312770, 768444, 93326, 16802263600 )
# plot('sp_1.0.rt.txt', 'sp_10.0.rt.txt', 'sp_100.0.rt.txt', 'sp_1000.0.rt.txt', 'sp_10000.0.rt.txt', 'sp.rt.txt', 29003463, 28689526, 19057274, 10739375, 2424127, 299401809195 )
# plot('ua_1.0.rt.txt', 'ua_10.0.rt.txt', 'ua_100.0.rt.txt', 'ua_1000.0.rt.txt', 'ua_10000.0.rt.txt', 'ua.rt.txt', 30246294, 24895423, 10900136, 5462679, 841925, 244519909705 )

# append(stry_axis_1(math.log(float(line[0])+1)))