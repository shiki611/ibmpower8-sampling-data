from funcs import read_file, fn_to_array,plot_pre, plot_mr, plot_mr_complete, plot_fp_complete, plot_rt_complete



# all_plots in paper
# footprint
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'bt', 2, 70000000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'cg', 2, 20000000,1400000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'ft', 2, 15000000, 2500000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'lu', 2, 38000000, 1100000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'mg', 2, 2000000, 750000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'sp', 2, 22000000)
# plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'ua', 2, 12000000, 750000)
plot_fp_complete("C://ibmpower8-sampling-data//plot script//footprint", 'is', 2, 600000, 140000)

# missratio
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'bt', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'cg', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'ft', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'is', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'lu', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'mg', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'sp', 1, 8)
# plot_mr_complete("C://ibmpower8-sampling-data//plot script//miss ratio", 'ua', 1, 8)

