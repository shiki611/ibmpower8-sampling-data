import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import math
import os


def read_file(filename):

    with open(filename) as h:
        lines = h.readlines()
	x_axis = []
    y_axis = []
    for i in lines:
    	line = i.split()
    	# print float(line[0])
    	# print float(line[0].rstrip(('\n')))
    	x_axis.append(float(line[0]))
        y_axis.append(float(line[1]))
    x_axis = np.array(x_axis)
    y_axis = np.array(y_axis)
    return [x_axis, y_axis]
    h.close()

def fn_to_array(path, keyword = None):
	path_list = []
	sub = os.listdir(path)
	subdir = filter(lambda x: keyword in x, sub)
	for i in subdir:
		if ".txt" in i:
			print i
			path_list.append(path + "//" + i)
	# print path_list
	return path_list

def plot_pre(array):
	# print len(array)
	lists = [[] for _ in range(len(array))]
	# print len(lists)
	for i in array:
		# if "_1.0" in i:
		# 	lists[0].append(read_file(i))
		# if "_10.0" in i:
		# 	lists[1].append(read_file(i))
		# if "_100.0" in i:
		# 	lists[2].append(read_file(i))
		# if "_1000.0" in i:
		# 	lists[3].append(read_file(i))
		# if "_10000.0" in i:
		# 	lists[4].append(read_file(i))
		# if "vfp." in i:
		# 	lists[6].append(read_file(i))
		# else:
		# 	lists[5].append(read_file(i))


		# if ".1000." in i:
		# 	lists[0].append(read_file(i))
		# if ".5000." in i:
		# 	lists[1].append(read_file(i))
		# if ".10000." in i:
		# 	lists[2].append(read_file(i))
		# if ".15000." in i:
		# 	lists[3].append(read_file(i))
		# if "_100.0" in i:
		# 	lists[0].append(read_file(i))
		# if "1000.avg" in i:
		# 	lists[1].append(read_file(i))
		# if ".5000.avg" in i:
		# 	lists[2].append(read_file(i))
		# if ".10000.avg" in i:
		# 	lists[3].append(read_file(i))
		# if ".15000.avg" in i:
		# 	lists[4].append(read_file(i))
		# if "vfp." in i:
		# 	lists[5].append(read_file(i))
		# if "filter." in i:
		# 	lists[6].append(read_file(i))
		# if "fulltrace." in i:
		# 	lists[7].append(read_file(i))

		# fp
		# if "_100.0" in i:
		# 	lists[0].append(read_file(i))
		# if ".5000.avg" in i:
		# 	lists[1].append(read_file(i))
		# if ".10000.avg" in i:
		# 	lists[2].append(read_file(i))
		# if ".15000.avg" in i:
		# 	lists[3].append(read_file(i))
		# miss ratio
		if ".100." in i:
			lists[0].append(read_file(i))
		if ".5000.avg" in i:
			lists[1].append(read_file(i))
		if ".10000.avg" in i:
			lists[2].append(read_file(i))
		if ".15000.avg" in i:
			lists[3].append(read_file(i))
	print lists[0]
	# print len(lists)
	# print lists
	return lists

def plot_mr(lists, keyword = None, ln = None):
	# print "----------------------------------------------\n"
	# print len(lists)
	# for i in range(len(lists)):
		# plt.plot(lists[i][0][0] * 128 / pow(2,20), lists[i][0][1], linewidth=2)
	plt.figure(figsize=(10.0, 7.5))
	plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower', nbins = 10))
	# plt.gca().yaxis.set_major_locator(MaxNLocator(prune='lower', nbins=12))
	plt.gca().xaxis.offsetText.set_fontsize(18)
	plt.gca().yaxis.offsetText.set_fontsize(18)
	plt.plot(lists[0][0][0] * 128 / pow(2,20), lists[0][0][1], c='g', marker = 'o', markevery=50, linewidth=2)
	plt.plot(lists[1][0][0] * 128 / pow(2,20), lists[1][0][1], c='b', linestyle = '--', marker = '*', markevery=50, linewidth=2)
	plt.plot(lists[2][0][0] * 128 / pow(2,20), lists[2][0][1], c='r', linestyle = '-.', marker = '^', markevery=50, linewidth=2)
	plt.plot(lists[3][0][0] * 128 / pow(2,20), lists[3][0][1], c='y', linestyle = ':', marker = 's', markevery=50, linewidth=2)
	plt.legend(['100', ' 5k', '10k', '15k'], loc= ln, prop={'size':14})
	plt.xlabel(r'$Cache$' + ' ' +r'$size(Mb)$', fontsize = 28)
	plt.ylabel(r'$Miss$' + ' ' +r'$ratio$', fontsize = 28)

def plot_fp(lists, keyword = None, ln = None):
	# for i in range(len(lists)):
		# plt.plot(lists[i][0][0], lists[i][0][1], linewidth=2)
	# plt.legend(['   1/100', '  1/5000', ' 1/10000', ' 1/15000'], loc='upper left', prop={'size':10})
	plt.figure(figsize=(10.0, 7.5))
	plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower', nbins = 10))
	# plt.gca().yaxis.set_major_locator(MaxNLocator(prune='lower', nbins=12))
	plt.gca().xaxis.offsetText.set_fontsize(18)
	plt.gca().yaxis.offsetText.set_fontsize(18)
	plt.plot(lists[0][0][0], lists[0][0][1], c='g', marker = 'o', markevery=50, linewidth=2)
	plt.plot(lists[1][0][0], lists[1][0][1], c='b', linestyle = '--', marker = '*', markevery=50, linewidth=2)
	plt.plot(lists[2][0][0], lists[2][0][1], c='r', linestyle = '-.', marker = '^', markevery=50, linewidth=2)
	plt.plot(lists[3][0][0], lists[3][0][1], c='y', linestyle = ':', marker = 's', markevery=50, linewidth=2)
	plt.legend(['100', ' 5k', '10k', '15k'], loc= ln, prop={'size':14})
	plt.xlabel(r'$Window$' + ' ' +r'$size$', fontsize = 28)
	# plt.xlabel(r'$window size$')
	plt.ylabel(r'$Footprint$', fontsize = 28)

def plot_rt(lists, keyword, array):
	for i in range(len(lists)):
		plt.plot(lists[i][0][0], lists[i][0][1], linewidth=2)

	# print sum(lists[i][0][1]/sum(lists[i][0][1]))
		# plt.plot(math.log(lists[i][0][0]), lists[i][0][1] * array[-1] / array[i], linewidth=2)
	plt.legend(['        1/1', '      1/10', '    1/100', '  1/1000', '1/10000', 'full trace'], loc='upper right', prop={'size':10})
	plt.xlabel('window size')
	plt.ylabel('access')
	plt.ylim(0,0.4)

def plot_mr_complete(path, keyword, ln = 2, xlim = None):
	path_list = fn_to_array(path, keyword)
	plot_array = plot_pre(path_list)
	plot_mr(plot_array, keyword, ln)
	plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
	plt.xticks(fontsize = 20)
	plt.yticks(fontsize = 20)
	plt.xlim(0, xlim)
	plt.ylim(0, 1)
	plt.title(keyword.upper(), fontsize=28)
	plt.savefig(keyword + '_mr' + '.pdf')
	plt.show()

def plot_fp_complete(path, keyword, ln = 2, xlim = None, ylim = None):
	path_list = fn_to_array(path, keyword)
	plot_array = plot_pre(path_list)
	plot_fp(plot_array, keyword, ln)
	# plt.yaxis.set_major_formatter(mpl.ticker.ScalarFormatter(useMathText=True))
	plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
	plt.xticks(fontsize = 20)
	plt.yticks(fontsize = 20)
	plt.xlim(0, xlim)
	plt.ylim(0, ylim)
	plt.title(keyword.upper(), fontsize=28)
	plt.savefig(keyword + '_fp' + '.pdf')
	plt.show()

# array is the access of specified sampling ratio: 0. 1, 1. 10, 2. 100, 3. 1000, 4.10000, 5. full trace
def plot_rt_complete(path, keyword, array = None):
	path_list = fn_to_array(path, keyword)
	plot_array = plot_pre(path_list)
	plot_rt(plot_array, keyword = None, array = None)
	plt.show()