import matplotlib.pyplot as plt
import math
import numpy as np

def plot(fn1000, fn10k, fn100k, fnft):
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0])+1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    h.close()

    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10k.append(str(math.log(float(line[0])+1, 2)))
        y_axis_10k.append((float(line[1].rstrip(('\n')))))
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    pre_y = []
    for i in lines:
        # print i
        line = i.split('\t')
        # print line[0]
        x_axis_100k.append((math.log(float(line[0])+1, 2)))
        y_axis_100k.append((float(line[1].rstrip(('\n')))))

    # for i in x_axis_100k:
    #     pre_y.append(80000 * math.log(i + 1))
    # print 'max =', max(float(x_axis_100k))
    # print 'min =', min(float(x_axis_100k))
    g.close()

    with open(fnft) as h:
        lines = h.readlines()

    x_axis_ft = []
    y_axis_ft = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_ft.append(str(math.log(float(line[0])+1, 2)))
        y_axis_ft.append((float(line[1].rstrip(('\n')))))
    h.close()

    pre_x = np.arange(0, 60000000000000000000000000000, 100000000000000000000000)
    new_pre_x = []
    pre_y = []
    for i in pre_x:
        new_pre_x.append(math.log(i + 1) - 39.5)
    pre_y = []
    for i in pre_x:
        pre_y.append(math.pow(i, 0.45)/1000000)
        # pre_y.append( i * i * i * i * i /1000)
    print pre_x[0], pre_x[1]



    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, y_axis_100k, linewidth=2)
    plt.plot(x_axis_ft, y_axis_ft, linewidth=2)
    plt.plot(new_pre_x, pre_y, linewidth = 2)
    # plt.semilogx(pre_x, math()
    # plt.plot(x_axis_100k, pre_y, linewidth = 2)

    plt.legend(['1k', '10k', '100k', 'full trace'], loc='upper left')
    plt.title("fp-" + fn10k[0] + fn10k[1], fontsize=30)
    # xticks(x_axis_1000)
    plt.xlabel('window size (logw)')
    plt.ylabel('access')
    # plt.ylim((0,1500000))
    # plt.xlim((0,35))
    # plt.savefig('fp_ft_' + fn10k[0] + fn10k[1] + '.pdf')
    plt.show()



plot('bt_1000.0_fp.txt', 'bt_10k.0_fp.txt', 'bt_100k.0_fp.txt', 'bt.fp.txt')
plot('cg_1000.0_fp.txt', 'cg_10k.0_fp.txt', 'cg_100k.0_fp.txt', 'cg.fp.txt')
plot('dc_1000.0_fp.txt', 'dc_10k.0_fp.txt', 'dc_100k.0_fp.txt', 'dc.fp.txt')
plot('ft_1000.0_fp.txt', 'ft_10k.0_fp.txt', 'ft_100k.0_fp.txt', 'ft.fp.txt')
plot('is_1000.0_fp.txt', 'is_10k.0_fp.txt', 'is_100k.0_fp.txt', 'is.fp.txt')
plot('lu_1000.0_fp.txt', 'lu_10k.0_fp.txt', 'lu_100k.0_fp.txt', 'lu.fp.txt')
plot('mg_1000.0_fp.txt', 'mg_10k.0_fp.txt', 'mg_100k.0_fp.txt', 'mg.fp.txt')
plot('ua_1000.0_fp.txt', 'ua_10k.0_fp.txt', 'ua_100k.0_fp.txt', 'ua.fp.txt')
plot('sp_1000.0_fp.txt', 'sp_10k.0_fp.txt', 'sp_100k.0_fp.txt', 'sp.fp.txt')