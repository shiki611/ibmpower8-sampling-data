import matplotlib.pyplot as plt
import math

# def plot(fn1000, fn10k, fn100k, ratio1, ratio2):
def plot(fn1000, fn10k, fn100k, fnft, acc_1000, acc_10k, acc_100k, acc_ft):
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []
    nor_y_axis_1000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0]) * acc_ft / acc_1000 +1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    di_1000 = y_axis_1000[0]
    # nor_y_axis_1000 = y_axis_1000/sum_1000
    for j in y_axis_1000:
        nor_y_axis_1000.append(j / di_1000)
    # print sum_1000
    # print y_axis_1000
    # print "-----------"
    # print "sum1000 =" , sum(nor_y_axis_1000)

    h.close()

    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []
    nor_y_axis_10k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10k.append(str(math.log(float(line[0]) * acc_ft / acc_10k +1, 2)))
        # x_axis_10k.append(str(math.log(float(line[0]) +1, 2)))
        y_axis_10k.append((float(line[1].rstrip(('\n')))))
    di_10k = y_axis_10k[0]
    # print y_axis_10k[0]
    for j in y_axis_10k:
        # j = j / sum_10k
        nor_y_axis_10k.append(j / di_10k)
    # nor_y_axis_10k = y_axis_10k/sum_10k
    # print "sum10k = ", sum(nor_y_axis_10k)
    print 'nor_y_axis_10k = [0]', nor_y_axis_10k[0]
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    nor_y_axis_100k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100k.append(str(math.log(float(line[0]) * acc_ft / acc_100k +1, 2)))
        # x_axis_100k.append(str(math.log(float(line[0]) +1, 2)))
        y_axis_100k.append((float(line[1].rstrip(('\n')))))
    di_100k = y_axis_100k[0]
    # nor_y_axis_100k = y_axis_100k/sum_10k
    # print sum_100k
    for j in y_axis_100k:
        nor_y_axis_100k.append(j / di_100k)
    # print "----------------------"
    # print "sum100k = " , sum(nor_y_axis_100k)
    g.close()

    with open(fnft) as h:
        lines = h.readlines()

    x_axis_ft = []
    y_axis_ft = []
    nor_y_axis_ft = []
    acc_nor_y_axis_ft = []
    acc_sum = 1.0
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_ft.append(str(math.log(float(line[0]) +1, 2)))
        y_axis_ft.append((float(line[1].rstrip(('\n')))))
    sum_ft = sum(y_axis_ft)
    # nor_y_axis_100k = y_axis_100k/sum_10k
    print sum_ft
    for j in y_axis_ft:
        nor_y_axis_ft.append(j / sum_ft)
    print "----------------------"
    print 'sum_y_ft =', sum(nor_y_axis_ft)
    print '0 , 1 = ', nor_y_axis_ft[0], nor_y_axis_ft[1]
    for k in nor_y_axis_ft:
        acc_nor_y_axis_ft.append(acc_sum - k)
        acc_sum = acc_sum - k
    h.close()


    plt.plot(x_axis_1000, nor_y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, nor_y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, nor_y_axis_100k, linewidth=2)
    plt.plot(x_axis_ft, acc_nor_y_axis_ft, linewidth=2)


    plt.legend(['1k', '10k', '100k', 'full trace'], loc='upper right')
    plt.title("RT-ns-acc-" + fn10k[0] + fn10k[1], fontsize=30)
    # xticks(x_axis_1000)
    plt.xlabel('window size (logw)')
    plt.ylabel('ratio(access/total access)')
    plt.savefig('rt_ft_ns_acc_' + fn10k[0] + fn10k[1] + '.pdf')
    plt.show()



plot('bt_1000.0_rt.txt', 'bt_10k.0_rt.txt', 'bt_100k.0_rt.txt', 'bt.rt.txt', 569655, 5649150, 26563613, 920531000877)
plot('cg_1000.0_rt.txt', 'cg_10k.0_rt.txt', 'cg_100k.0_rt.txt', 'cg.rt.txt', 196932, 1969661, 18063569, 88293856201)
plot('dc_1000.0_rt.txt', 'dc_10k.0_rt.txt', 'dc_100k.0_rt.txt', 'dc.rt.txt', 1150428, 11129073, 115980806, 332289621654) 
# # plot('ep_1000.0_rt.txt', 'ep_10k.0_rt.txt', 'ep_100k.0_rt.txt', 'ep.rt.txt', 338935, 2522682, 26148061, )
plot('ft_1000.0_rt.txt', 'ft_10k.0_rt.txt', 'ft_100k.0_rt.txt', 'ft.rt.txt', 91061, 837237, 6741236, 79262791475)
plot('is_1000.0_rt.txt', 'is_10k.0_rt.txt', 'is_100k.0_rt.txt', 'is.rt.txt', 9876, 94916, 1365007, 5389788804)
plot('lu_1000.0_rt.txt', 'lu_10k.0_rt.txt', 'lu_100k.0_rt.txt', 'lu.rt.txt', 222896, 1593635, 19623982, 351185143333)
plot('mg_1000.0_rt.txt', 'mg_10k.0_rt.txt', 'mg_100k.0_rt.txt', 'mg.rt.txt', 6885, 85188, 1131202, 16802263600)
plot('sp_1000.0_rt.txt', 'sp_10k.0_rt.txt', 'sp_100k.0_rt.txt', 'sp.rt.txt', 152932, 1044987, 7151435, 299401809195)
plot('ua_1000.0_rt.txt', 'ua_10k.0_rt.txt', 'ua_100k.0_rt.txt', 'ua.rt.txt', 64366, 606495, 6173969, 244519909705)



# append(stry_axis_1000(math.log(float(line[0])+1)))