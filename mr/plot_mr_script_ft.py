import matplotlib.pyplot as plt

def plot(fn1000, fn10k, fn100k, fnft, l1_mr):
    
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_1000.append(line[1].rstrip(('\n')))
    print "1k mr = " + y_axis_1000[-1]
    h.close()
    
    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []

    for i in lines:
            # print i
        line = i.split('\t')
        x_axis_10k.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_10k.append(line[1].rstrip(('\n')))
    print "10k mr = " + y_axis_10k[-1]
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100k.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_100k.append(line[1].rstrip(('\n')))
    print "100k mr = " + y_axis_100k[-1]
    g.close()

    with open(fnft) as h:
        lines = h.readlines()

    x_axis_ft = []
    y_axis_ft = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_ft.append(str(float(line[0].rstrip(('\n'))) / 16384))
        # y_axis_ft.append(float(line[1].rstrip(('\n'))) / l1_mr)
        y_axis_ft.append(str(float(line[1].rstrip(('\n'))) / l1_mr))

    print "ft mr = " , y_axis_ft[0], y_axis_ft[-1]
    h.close()

    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, y_axis_100k, linewidth=2)
    plt.plot(x_axis_ft, y_axis_ft, linewidth=2)

    plt.legend(['1k', '10k', '100k','full trace'], loc='upper right')
    plt.title('mr-' + fn10k[0]+fn10k[1], fontsize=30)
    plt.xlabel('cache size (Mb)')
    plt.ylabel('miss ratio')
    # xticks(x_axis_1000)
    plt.savefig('mr_ft_' + fn10k[0]+fn10k[1]+ '.pdf')
    plt.ylim((0,1))
    plt.show()



plot('bt_1000.0_mr.txt', 'bt_10k.0_mr.txt', 'bt_100k.0_mr.txt', 'bt.mr.txt', 0.076196)
plot('cg_1000.0_mr.txt', 'cg_10k.0_mr.txt', 'cg_100k.0_mr.txt', 'cg.mr.txt', 0.298450)
plot('dc_1000.0_mr.txt', 'dc_10k.0_mr.txt', 'dc_100k.0_mr.txt', 'dc.mr.txt', 0.060993)
plot('ft_1000.0_mr.txt', 'ft_10k.0_mr.txt', 'ft_100k.0_mr.txt', 'ft.mr.txt', 0.534124)
plot('is_1000.0_mr.txt', 'is_10k.0_mr.txt', 'is_100k.0_mr.txt', 'is.mr.txt', 0.081163)
plot('lu_1000.0_mr.txt', 'lu_10k.0_mr.txt', 'lu_100k.0_mr.txt', 'lu.mr.txt', 0.168841)
plot('mg_1000.0_mr.txt', 'mg_10k.0_mr.txt', 'mg_100k.0_mr.txt', 'mg.mr.txt', 0.052396)
plot('sp_1000.0_mr.txt', 'sp_10k.0_mr.txt', 'sp_100k.0_mr.txt', 'sp.mr.txt', 0.080898)
plot('ua_1000.0_mr.txt', 'ua_10k.0_mr.txt', 'ua_100k.0_mr.txt', 'ua.mr.txt', 0.030649)
