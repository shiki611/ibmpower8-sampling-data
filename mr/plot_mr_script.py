import matplotlib.pyplot as plt

def plot(fn1000, fn10k, fn100k):
    
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_1000.append(line[1].rstrip(('\n')))
    print "1k mr = " + y_axis_1000[-1]
    h.close()
    
    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []

    for i in lines:
            # print i
        line = i.split('\t')
        x_axis_10k.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_10k.append(line[1].rstrip(('\n')))
    print "10k mr = " + y_axis_10k[-1]
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100k.append(str(float(line[0].rstrip(('\n'))) / 16384))
        y_axis_100k.append(line[1].rstrip(('\n')))
    print "100k mr = " + y_axis_100k[-1]
    g.close()

    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, y_axis_100k, linewidth=2)

    plt.legend(['1k', '10k', '100k'], loc='upper right')
    plt.title('mr-' + fn10k[0]+fn10k[1], fontsize=30)
    plt.xlabel('cache size (Mb)')
    plt.ylabel('miss ratio')
    # xticks(x_axis_1000)
    # plt.savefig('mr_' + fn10k[0]+fn10k[1]+ '.pdf')
    plt.show()



plot('bt_1000.0_mr.txt', 'bt_10k.0_mr.txt', 'bt_100k.0_mr.txt')
