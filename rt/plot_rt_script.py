import matplotlib.pyplot as plt
import math

def plot(fn1000, fn10k, fn100k):
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0])+1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    h.close()

    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []

    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10k.append(str(math.log(float(line[0])+1, 2)))
        y_axis_10k.append((float(line[1].rstrip(('\n')))))
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100k.append(str(math.log(float(line[0])+1, 2)))
        y_axis_100k.append((float(line[1].rstrip(('\n')))))
    g.close()

    plt.plot(x_axis_1000, y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, y_axis_100k, linewidth=2)

    plt.legend(['1k', '10k', '100k'], loc='upper right')
    plt.title("rt-" + fn10k[0] + fn10k[1], fontsize=30)
    # xticks(x_axis_1000)
    plt.xlabel('window size (logw)')
    plt.ylabel('access')
    plt.savefig('rt_' + fn10k[0] + fn10k[1] + '.pdf')
    plt.show()


plot('ua_1000.0_rt.txt', 'ua_10k.0_rt.txt', 'ua_100k.0_rt.txt')

# append(str(math.log(float(line[0])+1)))