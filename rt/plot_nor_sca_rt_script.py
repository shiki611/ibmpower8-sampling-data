import matplotlib.pyplot as plt
import math

def plot(fn1000, fn10k, fn100k, ratio1, ratio2):
    with open(fn1000) as h:
        lines = h.readlines()

    x_axis_1000 = []
    y_axis_1000 = []
    nor_y_axis_1000 = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_1000.append(str(math.log(float(line[0]) * ratio1 +1, 2)))
        y_axis_1000.append((float(line[1].rstrip(('\n')))))
    sum_1000 = sum(y_axis_1000)
    # nor_y_axis_1000 = y_axis_1000/sum_1000
    for j in y_axis_1000:
        nor_y_axis_1000.append(j / sum_1000)
    print sum_1000
    # print y_axis_1000
    print "-----------"
    print sum(nor_y_axis_1000)

    h.close()

    with open(fn10k) as f:
        lines = f.readlines()

    x_axis_10k = []
    y_axis_10k = []
    nor_y_axis_10k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_10k.append(str(math.log(float(line[0]) * ratio2 +1, 2)))
        y_axis_10k.append((float(line[1].rstrip(('\n')))))
    sum_10k = sum(y_axis_10k)
    print sum_10k
    for j in y_axis_10k:
        # j = j / sum_10k
        nor_y_axis_10k.append(j / sum_10k)
    # nor_y_axis_10k = y_axis_10k/sum_10k
    print sum(nor_y_axis_10k)
    f.close()

    with open(fn100k) as g:
        lines = g.readlines()

    x_axis_100k = []
    y_axis_100k = []
    nor_y_axis_100k = []
    for i in lines:
        # print i
        line = i.split('\t')
        x_axis_100k.append(str(math.log(float(line[0]) +1, 2)))
        y_axis_100k.append((float(line[1].rstrip(('\n')))))
    sum_100k = sum(y_axis_100k)
    # nor_y_axis_100k = y_axis_100k/sum_10k
    print sum_100k
    for j in y_axis_100k:
        nor_y_axis_100k.append(j / sum_100k)
    print "----------------------"
    print sum(nor_y_axis_100k)
    g.close()

    plt.plot(x_axis_1000, nor_y_axis_1000, linewidth=2)
    plt.plot(x_axis_10k, nor_y_axis_10k, linewidth=2)
    plt.plot(x_axis_100k, nor_y_axis_100k, linewidth=2)

    plt.legend(['1k', '10k', '100k'], loc='upper right')
    plt.title("RT-ns-" + fn10k[0] + fn10k[1], fontsize=30)
    # xticks(x_axis_1000)
    plt.xlabel('window size (logw)')
    plt.ylabel('ratio(access/total access)')
    plt.savefig('rt_ns_' + fn10k[0] + fn10k[1] + '.pdf')
    plt.show()



plot('bt_1000.0_rt.txt', 'bt_10k.0_rt.txt', 'bt_100k.0_rt.txt', 46.63105388, 4.70223184)
plot('cg_1000.0_rt.txt', 'cg_10k.0_rt.txt', 'cg_100k.0_rt.txt', 91.72490504, 9.170902506)
plot('dc_1000.0_rt.txt', 'dc_10k.0_rt.txt', 'dc_100k.0_rt.txt', 100.8153539, 10.42142558)
plot('ep_1000.0_rt.txt', 'ep_10k.0_rt.txt', 'ep_100k.0_rt.txt', 77.14771564, 10.36518317)
plot('ft_1000.0_rt.txt', 'ft_10k.0_rt.txt', 'ft_100k.0_rt.txt', 74.02989205, 8.05176551)
plot('is_1000.0_rt.txt', 'is_10k.0_rt.txt', 'is_100k.0_rt.txt', 138.2145606, 14.38121075)
plot('lu_1000.0_rt.txt', 'lu_10k.0_rt.txt', 'lu_100k.0_rt.txt', 88.04097875, 12.31397528)
plot('mg_1000.0_rt.txt', 'mg_10k.0_rt.txt', 'mg_100k.0_rt.txt', 164.2994916, 13.27888905)
plot('sp_1000.0_rt.txt', 'sp_10k.0_rt.txt', 'sp_100k.0_rt.txt', 46.76218842, 6.843563604)
plot('ua_1000.0_rt.txt', 'ua_10k.0_rt.txt', 'ua_100k.0_rt.txt', 95.9197247, 10.17975251)

# append(stry_axis_1000(math.log(float(line[0])+1)))