#include <iostream>

#include "footprint.h"
#include "footprintManager.h"
#include "hist.h"

using namespace std;

int main(int argc, char** argv){
    if(argc != 3){
        cerr << "Usage : command fp rt" << endl;
        return 1;
    }

    FPDist* fp = (FPDist::deserialize(argv[1]));
    ifstream fin(argv[2]);
    HashHist<uint64_t, uint64_t>* rthist = (HashHist<uint64_t,uint64_t>::deserialize(fin));
    fin.close();

    for (int i = 0; i <= 1048576; i += 128)
    //for(int i = 0; i < 128; i++)
        cout << i << '\t' << FPUtil::GetMissRatio(fp, rthist, (double)i) << "\n";
    
    return 0;
}
