#include <iostream>
#include <string>
#include <map>
#include <unordered_map>
#include <cstdint>

#include "TraceManager.h"
using namespace std;

unordered_map<uint64_t, uint64_t> acount;
map<uint64_t, uint64_t> ncount;
int main(int argc, char** argv){
    TraceReader tr;
    string trace = argv[1];

    if (tr.open(trace.c_str())) {
        MEMREF mf;
        while (tr.next(mf)) {
            acount[mf.address >> 7]++;
        }
    }

    for(auto i : acount)
        ncount[i.second]++;
    for(auto i : ncount)
        cout << i.first << "\t" << i.second << endl;
    return 0;
}
