#pragma once

#include <cstdint>
#include <vector>
#include <algorithm>

using namespace std;

typedef uint64_t AddrInt;

class LRUSet{
private:
    int associative;
    AddrInt *slots;
    int *time_label;
    int tick;
public:
    LRUSet(int asso){
        associative = asso;
        slots = new AddrInt[associative];
        time_label = new int[associative];
        fill(time_label, time_label+associative, -1);
        tick = 0;
    }

    bool Access(AddrInt addr){
        tick++;
        
        int idx = 0;
        //Two purpose : check existence, find LRU bit
        for(int i = 0; i < associative; i++){
            if(time_label[i] != -1 && slots[i] == addr){
                time_label[i] = tick;
                return true;
            }
            if(time_label[i] < time_label[idx])
                idx = i;
        }
        slots[idx] = addr;
        time_label[idx] = tick;
        return false;
    }
};

class LRUCache{
private:
    uint64_t capacity;
    int line;
    int associative;
    int nsets;
    vector<LRUSet> sets;
public:
    LRUCache(int cap, int l, int asso){
        capacity = cap;
        line = l;
        associative = asso;
        nsets = cap/l/associative;
        for(int i = 0; i < nsets; i++)
            sets.push_back(LRUSet(associative));
    }

    AddrInt getLineAddr(AddrInt addr){
        return addr/line;
    }

    AddrInt getSet(AddrInt addr){
        return getLineAddr(addr)%nsets;
    }

    bool Access(AddrInt addr){
        AddrInt line_addr = getLineAddr(addr);
        AddrInt set_idx = getSet(addr);
        return sets[set_idx].Access(line_addr); 
    }
};
