#include <string>

#include "footprint.h"
#include "footprintManager.h"
#include "TraceManager.h"

using namespace std;

double footp(uint64_t x, uint64_t m, HashHist<uint64_t, uint64_t>* &rt) {
    double n = 0;
    auto vecX = rt->GetXValues();
    auto vecY = rt->GetYValues();
    for (auto y : vecY)
        n += y;
    auto idx = rt->GetXID(x);
    double sum = 0;
    for (auto i = idx + 1; i < vecX.size(); i++) {
        sum += (vecX[i] - x) * vecY[i];
    }
    return m - (sum / (n - x + 1));
}

int main(int argc, char** argv){
    TraceReader tr;
    FPBuilder fpb;
    string trace = argv[1];
    string fname = trace;
    if(argc > 2) fname = argv[2];

    if (tr.open(trace.c_str())) {
        MEMREF mf;
        while (tr.next(mf)) {
            fpb.OnReference((mf.address  >> 7) << 7);
        }
        FPDist* fpd = fpb.Finalize();

        //serialization
        FPUtil::serialize(fpd, fname+".fp");
        ofstream fout(fname+".rt");
        fpb.rthist.serialize(fout);
    }

    FPDist* fp = (FPDist::deserialize(fname + ".fp"));
    uint64_t m = FPUtil::GetNData(fp);

    ifstream fin(fname + ".rt");
    HashHist<uint64_t, uint64_t>* rthist = (HashHist<uint64_t,uint64_t>::deserialize(fin));
    fin.close();


    ofstream out(fname + "_fp.txt");

    auto vecX = rthist->GetXValues();
    auto vecY = rthist->GetYValues();
    for (uint64_t i = 0; i < vecX.size(); i += 1) {
         out << vecX[i] << '\t' << fixed << footp(i, m, rthist) << '\n';
        //  cout << vecX[i] << '\t' << vecY[i] << '\n';
    }
    out.close();
    //  cout << vecX.size() << '\t' << vecY.size() << '\n';
    return 0;
}
