#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 10:03:49 2017

@author: mayer
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


k = np.fromfile("bt_100k.0.txt")
k10 = np.fromfile("bt_10k.0.txt")
k100 = np.fromfile("bt_1000.0.txt")

plt.plot(k)
plt.show()