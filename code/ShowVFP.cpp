#include <iostream>
#include <string>

#include "footprint.h"
#include "footprintManager.h"

using namespace std;

int main(int argc, char** argv){
    string fname(argv[1]);
    double l1m = atof(argv[2]);

    FPDist* fp = (FPDist::deserialize(fname));
    //ifstream fin(fname+".rt");
    //HashHist<uint64_t, uint64_t>* rthist = (HashHist<uint64_t,uint64_t>::deserialize(fin));
    //fin.close();

    const double l1size = 64*1024/128;
    double th = FPUtil::GetFT(fp, l1size);
    
    auto xval = fp->FPHist.GetXValues();
    for(auto i : xval)
        cout << i << " " << FPUtil::GetFP(fp, i/l1m+th)-l1size << endl;

    return 0;
}
