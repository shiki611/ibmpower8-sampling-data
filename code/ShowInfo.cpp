#include <iostream>

#include "footprint.h"
#include "footprintManager.h"
#include "hist.h"

using namespace std;

int main(int argc, char** argv){
    if(argc != 2){
        cerr << "Usage : command fp" << endl;
        return 1;
    }

    FPDist* fp = (FPDist::deserialize(argv[1]));
    
    cout << "Number of accesses: " << FPUtil::GetNAccesses(fp) << endl;
    cout << "Number of data: " << FPUtil::GetNData(fp) << endl;
    
    return 0;
}
