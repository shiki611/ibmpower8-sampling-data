#include <iostream>

#include "footprint.h"
#include "footprintManager.h"
#include "hist.h"

using namespace std;

int main(int argc, char** argv){
    if(argc != 2){
        cerr << "Usage : command rt" << endl;
        return 1;
    }

    ifstream fin(argv[1]);
    HashHist<uint64_t, uint64_t>* rthist = (HashHist<uint64_t,uint64_t>::deserialize(fin));
    fin.close();

    auto xval = rthist->GetXValues();
    auto yval = rthist->GetYValues();
    for(int i = 0; i < xval.size(); i++)
        cout << xval[i] << "\t" << yval[i] << endl;
    
    return 0;
}
